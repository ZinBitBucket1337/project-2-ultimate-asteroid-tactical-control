﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerZone : MonoBehaviour
{
    // Function definiton for OnTriggerEnter2D(Collider2D col).
    void OnTriggerEnter2D(Collider2D col)
    {
        // Whenever a game object touches the box colliders it will be destroyed.
        Destroy(col.gameObject);
    }
}
