﻿using UnityEngine;
using System.Collections;

public class FacesPlayer : MonoBehaviour {
    // The varible value in which the ship rotates (DESIGNER CAN MODIFY)
	public float rotSpeed = 90f;
    // Declare a trasform for the player ship.
	Transform player;

	// Update is called once per frame
	void Update () 
    {
        // An if statement for when the player is compared to null.
		if(player == null) 
        {
			// Find the player's ship!
			GameObject go = GameObject.FindWithTag ("Player");
            // An if statement for when go is not equal to null.
			if(go != null) 
            {
                // The player is equal to go.transform.
				player = go.transform;
			}
		}

        // An if statement for when the player is compared to null.
		if(player == null)
			return;	// Try again next frame!

        // A vector variable called dir that is equal to player.position subtracted by the transform.position.
		Vector3 dir = player.position - transform.position;
        // Return the magnitude of dir back to default.
		dir.Normalize();
        // The varaialbe zAngle is equal to the math function of the arc tangent with the parameters dir.y and dir.x which is then multiplied by the Rad2Deg minus 90.
		float zAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
        // Declare the quaternion variable desiredRot as equal to the rotation value of zAngle.
		Quaternion desiredRot = Quaternion.Euler( 0, 0,zAngle );
        // The transform.roation is equal to the rotation value within the parameter of transform.rotation, desiredRot, rotSpeed * Time.deltaTime. 
        transform.rotation = Quaternion.RotateTowards( transform.rotation, desiredRot, rotSpeed * Time.deltaTime);
	}
}
