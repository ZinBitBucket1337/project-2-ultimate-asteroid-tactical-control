﻿using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {
    // A variable that defines the max speed (DESIGNER CAN MODIFY)
	public float maxSpeed = 5f;

	// Update is called once per frame
	void Update () 
    {
        // A vector3 variable called pos which is equal to the value of transform.position.
		Vector3 pos = transform.position;
        // A vector3 variable called velcoity which is equal the value of the vector with the parameters (0, maxSpeed * Time.deltaTime, 0).
        Vector3 velocity = new Vector3(0, maxSpeed * Time.deltaTime, 0);
		// The value of pos is equal to the result of it added by transform.rotation * velocity.
		pos += transform.rotation * velocity;
        // The transform.position is equal to the variable pos.
		transform.position = pos;
	}
}
