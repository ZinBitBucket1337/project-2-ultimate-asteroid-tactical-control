﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteriodSpawner : MonoBehaviour
{
    // A variable that spawns the asteriod at a specific rate (DESIGNER CAN MODIFY).
    public float rate;
    // A array for the gameobject.
    public GameObject [] asteriods;

    // Start is called before the first frame update
    void Start()
    {
        // Invoke the method of spawning asteriods at a specific time and rate.
        InvokeRepeating("spawnAsteriod", rate, rate);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void spawnAsteriod()
    {
        // Create a clone of the asteriod at a randomized point at the x-axis.
        Instantiate(asteriods[(int)Random.Range(0,asteriods.Length)],new Vector3(Random.Range (-10.5f,10.5f),5.3f,0),Quaternion.identity);
    }
}
