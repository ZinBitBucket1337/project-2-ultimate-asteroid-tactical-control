﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour {
    // A vector varaible that declares the position of the bulletOffset. (DESIGNER CAN MODIFY)
	public Vector3 bulletOffset = new Vector3(0, 0.5f, 0);
	// Attachment for the gameObject which should be the bullet prefab. (DESIGNER CAN MODIFY)
	public GameObject bulletPrefab;
    // An int variable for the bullet layer.
	int bulletLayer;
    // A variable for the delay in which the rate of bullets will be fired. (DESIGNER CAN MODIFY)
	public float fireDelay = 0.50f;
    // A variable for the cooldown timer.
	float cooldownTimer = 0;
    // Declare a transform for the player ship.
	Transform player;


	void Start() 
    {
        // bullet layer is equal to the layer of the game object.
		bulletLayer = gameObject.layer;
	}

	// Update is called once per frame
	void Update () 
    {
        // An if statement for when the player ship is compared to null.
		if(player == null) 
        {
			// Find the player's ship.
			GameObject go = GameObject.FindWithTag ("Player");
			// An if statement for when go is not equal to null.
			if(go != null) 
            {
                // Player is equal to go.transform.
				player = go.transform;
			}
		}

        // The cooldownTimer will be the result of it being subtracted by Time.deltaTime.
		cooldownTimer -= Time.deltaTime;
		// An if statement for when the cool down timer is less than or equal to zero and the player object is not equl to null and the Vector distance with the parameters transform.position, player.position is less than four.
		if( cooldownTimer <= 0 && player != null && Vector3.Distance(transform.position, player.position) < 4) 
        {
	        // *Shoot*
			//Debug.Log ("Enemy Pew!");
			cooldownTimer = fireDelay;
			// Vector variable equal to the trasnforn.rotation multiplied by the bullet offset.
			Vector3 offset = transform.rotation * bulletOffset;
			// The game object for bulletGo is equal to the bullet prefab, with transform.position added with offset, and transform.rotation.
			GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, transform.position + offset, transform.rotation);
            // bulletGO layer is equal to the layer of the bullet object.
			bulletGO.layer = bulletLayer;
		}
	}
}
