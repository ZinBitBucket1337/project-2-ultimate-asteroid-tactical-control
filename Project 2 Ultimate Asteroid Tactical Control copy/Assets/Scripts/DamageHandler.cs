﻿using UnityEngine;
using System.Collections;

public class DamageHandler : MonoBehaviour {
    // int varaible representing the health of the ships. (DESIGNER CAN MODIFY)
	public int health = 1;
    // A public variable that provides the ships a time duration where they won't take damage. (DESIGNER CAN MODIFY)
	public float invulnPeriod = 0;
    // A float variable for the invulnerability timer.
	float invulnTimer = 0;
    // int variable to detect that the game objects are at the proper layering.
	int correctLayer;
    // Rendering the 2D image.
	SpriteRenderer spriteRend;


	void Start() 
    {
        // The int varaible is equal to the layer of the gameObject.
		correctLayer = gameObject.layer;

        // Rendering the parent object of the 2D sprite.
		spriteRend = GetComponent<SpriteRenderer>();
        // An if statement for whenever the varaible spriteRend is compared to null.
		if(spriteRend == null) 
        {
            // The spriteRend variable is equal to the transform component of the child with the sprite renderer.
			spriteRend = transform.GetComponentInChildren<SpriteRenderer>();
            // An if statement for whenever the varaible spriteRend is equallycompared to null.
            if (spriteRend==null) 
            {
                // Display a debug.log for the sprite Renderer. 
				Debug.LogError("Object '"+gameObject.name+"' has no sprite renderer.");
			}
		}
	}
    // A function defintion for OnTriggerEnter2D.
    void OnTriggerEnter2D() 
    {
        // Decrementing the health of the ships.
		health--;
        // An if statement that will be executed when invulnPeriod is greater than 0.
		if(invulnPeriod > 0) 
        {
            // The invulnerability timer is equal to the period of invulnerability.
            invulnTimer = invulnPeriod;
            // the layer of the gameObject will be equal to ten.
			gameObject.layer = 10;
		}
	}
    // Update is called once per frame
    void Update() 
    {
        // An if statement that will be executed when invulnTimer is greater than 0.
		if(invulnTimer > 0) 
        {
            // The invulnerability timer will be the result of it being subtracted from the Time.deltaTime.
			invulnTimer -= Time.deltaTime;
            // An if statement for when invulnTimer is less than or equal to zero.
			if(invulnTimer <= 0) 
            {
                // The layer of the gameObject is equal to the correctLayer.
				gameObject.layer = correctLayer;
                // An if statement for when the Sprite Render is not equal to null.
				if(spriteRend != null) 
                {
                    // The sprite render is enables with the boolean value of true.
					spriteRend.enabled = true;
				}
			}
            // An else statement for whenever the if statement is not proccessed.
			else 
            {
                // An if statement for when the sprite render is not equal to null.
				if(spriteRend != null) 
                {
                    // The spriteRend when enabled will equal to the not value of the spriteRend.
					spriteRend.enabled = !spriteRend.enabled;
				}
			}
		}
        // An if statement for when the health is less than or equal to zero.
		if(health <= 0) 
        {
            // Recalls the Die function
            Die();
		}
	}
    // Function definition of Die
	void Die() 
    {
        // Destroy's the gameobject.
		Destroy(gameObject);
	}

}
