﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    // The speed at which the player ships moves (DESIGNER CAN MODIFY).
	public float maxSpeed = 5f;
    // The speed at which the player ship can rotate (DESIGNER CAN MODIFY).
	public float rotSpeed = 180f;
    // The variable for the radius of the player ship boundary.
	float shipBoundaryRadius = 0.5f;

	void Start () 
    {
	
	}
	
	void Update () 
    {

		// ROTATE the ship.
		// Grab the rotation quaternion.
		Quaternion rot = transform.rotation;

		// Grab the rotation of the Z euler angle.
		float z = rot.eulerAngles.z;

		// Change the Z angle based on input.
		z -= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;

		// Recreate the quaternion.
		rot = Quaternion.Euler( 0, 0, z );

		// Feed the quaternion into our rotation.
		transform.rotation = rot;

		// Moving the ship.
		Vector3 pos = transform.position;
		 // Vector variable in charge of moving the player ship vertically.
		Vector3 velocity = new Vector3(0, Input.GetAxis("Vertical") * maxSpeed * Time.deltaTime, 0);
        // The position variable is added and equal to the rotation multiplied by the velocity.
		pos += rot * velocity;

        // Vertical movement
		// An if statement for when the y-axis position plus the ship boundary radius is greater than the orthographic camera size.
		if(pos.y+shipBoundaryRadius > Camera.main.orthographicSize) 
        {
            // The variable of the y-axis position is equal to the orthographic camera size minus the ship boundary radius.
			pos.y = Camera.main.orthographicSize - shipBoundaryRadius;
		}
        // An if statement for when the y-axis position minus the ship boundary radius is less than the orthographic camera size.
		if(pos.y-shipBoundaryRadius < -Camera.main.orthographicSize) 
        {
            // The variable of the y-axis position is equal to the negative value of the orthographic camera size plus the ship boundary size.
			pos.y = -Camera.main.orthographicSize + shipBoundaryRadius;
		}

		// Calculating the orthographic width based on the screen ratio.
		float screenRatio = (float)Screen.width / (float)Screen.height;
        // Calculating the orthographic width based on the screen ratio.
        float widthOrtho = Camera.main.orthographicSize * screenRatio;

        // Horizontal movement
		// An if statment for the x-axis position plus the ship boundary radius is greater than the width of the orthographic.
		if(pos.x+shipBoundaryRadius > widthOrtho) 
        {
            // The varaible called pos.x is equal to the width of the orthographic minus the ship boundary radius.
			pos.x = widthOrtho - shipBoundaryRadius;
		}
        // An if statement for when the x-axis position minus the ship boundary radius is less than the width of the orthographic.
		if(pos.x-shipBoundaryRadius < -widthOrtho) 
        {
            // The variable called pos.x is equal to the negative value of the width of the orthographic plus the ship boundary radius.
			pos.x = -widthOrtho + shipBoundaryRadius;
		}

		// update the position by haivng the transform.position equal to te varaivle pos.
		transform.position = pos;
	}
}
