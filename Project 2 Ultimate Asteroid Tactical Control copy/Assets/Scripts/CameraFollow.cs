﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
    // Create a transform for the varaible myTarget.
	public Transform myTarget;
	
	// Update is called once per frame
	void Update () 
    {
        // An if statement that will be executed whenever the target is not equal to "null".
		if(myTarget != null) 
        {
            // A declared variable called targPos which is equal to the position of the mytarget.
			Vector3 targPos = myTarget.position;
            // A target position for the z-axis.
			targPos.z = transform.position.z;
			// The transform position is equal to the vector variable targPos.
			transform.position = targPos;
		}
	}
}
