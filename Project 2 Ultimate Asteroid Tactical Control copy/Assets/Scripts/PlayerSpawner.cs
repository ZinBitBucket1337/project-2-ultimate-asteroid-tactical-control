﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour {

    // Attaches the player ship prefab to the PlayerSpawner Script.
	public GameObject playerPrefab;
    // Attaching the game object of the playerInstance.
	GameObject playerInstance;

    // The number of lives that the player ship has, and the designer is able to adjust the number of lives.
	public int numLives = 4;

    // Variable for the Respawn timer.
	float respawnTimer;

	// Use this for initialization
	void Start () 
    {
        // Function call to SpawnPlayer.
		SpawnPlayer();
	}

    // Function defintion of SpawnPlayer.
	void SpawnPlayer() 
    {
        // Decrement the number of lives
		numLives--;
        // Set respawnTimer to equal the value of one.
		respawnTimer = 1;
        // The playerInstance is equal to the the game object player prefab clone with the prefabs (transform.position, Quaternion.identity).
        playerInstance = (GameObject)Instantiate(playerPrefab, transform.position, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () 
    {
        // An if statement for when the playerInstance is compared to null and the number of lives in greater than zero.
		if(playerInstance == null && numLives > 0) 
        {
            // The respawn timer is equal to the result of it being subtracted by the Time.deltaTime.
			respawnTimer -= Time.deltaTime;
            // An if statement for when the respawn timer is less than or equal to zero.
			if(respawnTimer <= 0) 
            {
                // Function call to SpawnPlayer.
				SpawnPlayer();
			}
		}
	}
    // Function definition for OnGUI.
	void OnGUI() 
    {
        // An if statement for when the number of lives is greater than zero or the playerInstance is not equal to null.
		if(numLives > 0 || playerInstance!= null) 
        {
            // Disiplay the number of lives that the player has.
			GUI.Label( new Rect(0, 0, 100, 50), "Lives Left: " + numLives);
		}
        // An else statement for when the if condition isn't followed.
		else 
        {
            // Display a message to the player that the game is over and you lost all your lives.
			GUI.Label( new Rect( Screen.width/2 - 50 , Screen.height/2 - 25, 100, 50), "Game Over!");

		}
	}
}
