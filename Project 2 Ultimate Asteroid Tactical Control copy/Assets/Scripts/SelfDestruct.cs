﻿using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour {

    // Float variable for the timer.
	public float timer = 1f;

	void Update () 
    {
        // The timer variable is equal to the result of it being subtracted by Time.deltaTime.
		timer -= Time.deltaTime;
        // An if statement for when the timer is less than or equal to zero.
		if(timer <= 0) 
        {
            // Function call with parameter to destroy the game object.
			Destroy(gameObject);
		}
	}
}
