﻿ using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour {
    // A vector variable that declares the position of the bulletOffset. (DESIGNER CAN MODIFY)
	public Vector3 bulletOffset = new Vector3(0, 0.5f, 0);
    // Attachment for the gameObject which should be the bullet prefab. (DESIGNER CAN MODIFY)
    public GameObject bulletPrefab;
    // An int variable for the bullet layer.
    int bulletLayer;
    // A variable for the delay in which the rate of bullets will be fired. (DESIGNER CAN MODIFY)
    public float fireDelay = 0.25f;
    // A variable for the cooldown timer.
    float cooldownTimer = 0;

    void Start() 
    {
        // bullet layer is equal to the layer of the game object.
        bulletLayer = gameObject.layer;
	}

	// Update is called once per frame
	void Update () 
    {
        // The cooldownTimer will be the result of it being subtracted by Time.deltaTime.
        cooldownTimer -= Time.deltaTime; 
        // An if statement for when the fire button is pressed and the cooldownTimer is less than or equal to zero.
		if( Input.GetButton("Fire1") && cooldownTimer <= 0 ) 
        {
			// Fires off the bullets when the delay is not enabled.
			cooldownTimer = fireDelay;
            // The vector variable offset is equal to the transform.rotation multiplied by bulletOffset.
			Vector3 offset = transform.rotation * bulletOffset;
            // The game object for bulletGo is equal to the bullet prefab, with transform.position added with offset, and transform.rotation.
            GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, transform.position + offset, transform.rotation);
            // bulletGO layer is equal to the layer of the bullet object.
            bulletGO.layer = bulletLayer;
		}
	}
}
