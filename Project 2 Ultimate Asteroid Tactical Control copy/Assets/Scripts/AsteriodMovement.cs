﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteriodMovement : MonoBehaviour
{
    // Attaches the 2D rigid body physics.
    Rigidbody2D rb;
    // A variable for the x-axis speed of the asteriod (DESIGNER CAN MODIFY).
    public float xSpeed;
    // A variable for the y-axis speed of the asteriod (DESIGNER CAN MODIFY).
    public float ySpeed;

    private void Awake()
    {
        // Create's a variable called rb and collects and attaches the RigidBody2D.
        rb = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // The movement physics of the asteriod, ySpeed must be multiplied by negative one so it can move down.
        rb.velocity = new Vector2(xSpeed, ySpeed*-1);
    }
}
